#include <iostream>
#include <cg/service.hpp>
#include "lib/assert.hpp"

namespace tag
{
    struct Foo {};
}

template<int I>
struct A : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            int get()
            {
                if constexpr (Base::template has_remote<tag::Foo>)
                    return I + this->remote(tag::Foo{}).get();
                return I;
            }
        };
    };

    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                return this->local(tag::Foo{}).get();
            }
        };
    };

    using Ports = ct::map<ct::pair<tag::Foo, Impl>>;
    using Services = std::conditional_t<I == 1, ct::tuple<Service>, ct::tuple<>>;
};

int main()
{
    {
        cg::Host<cg::Graph<cg::Pipeline<A<1>, A<2>, A<3>, A<4>>>> host;
        ASSERT(cg::Service(host) == 10);
    }
    {
        cg::Host<cg::Graph<cg::Pipeline<A<1>, A<2>, A<3>, tag::Foo>>> host;
        ASSERT(cg::Service(host) == 6);
    }
    return result;
}
