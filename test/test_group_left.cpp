#include <cg/connect.hpp>
#include <cg/service.hpp>
#include "lib/assert.hpp"

namespace tag
{
    struct Foo {};
}

template<int I>
struct A : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            int get()
            {
                return I + this->remote(tag::Foo{}).get();
            }
        };
    };

    using Ports = ct::map<ct::pair<tag::Foo, Impl>>;
};

struct B : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            int get()
            {
                int result = 0;
                this->apply(tag::Foo{}, [&result](auto remote) mutable {
                    result += remote.get();
                });
                return result;
            }
        };
    };

    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                return this->local(tag::Foo{}).get();
            }
        };
    };
        
    using Ports = ct::map<ct::pair<tag::Foo, Impl>>;
    using Services = ct::tuple<Service>;
};

struct C : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            int get()
            {
                return 1;
            }
        };
    };

    using Ports = ct::map<ct::pair<tag::Foo, Impl>>;
};

int main()
{
    {
        using G = cg::Group<
            A<1>,
            A<2>,
            A<3>,
            A<4>
        >;
        cg::Host<cg::Graph<
            cg::Connect<B, G, tag::Foo>,
            cg::Connect<G, C, tag::Foo>
        >> host{};
        ASSERT(cg::Service(host) == 14);
    }
    {
        using G = cg::Group<
            A<1>,
            A<2>,
            A<3>,
            A<4>
        >;
        cg::Host<cg::Graph<
            cg::Connect<B, G>,
            cg::Connect<G, C>
        >> host{};
        ASSERT(cg::Service(host) == 14);
    }
    {
        using G = cg::Group<A<1>>;
        cg::Host<cg::Graph<
            cg::Connect<B, G>,
            cg::Connect<G, C>
        >> host{};
        ASSERT(cg::Service(host) == 2);
    }
    return result;
}
