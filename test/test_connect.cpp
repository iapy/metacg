#include <cg/connect.hpp>
#include <cg/service.hpp>
#include "lib/assert.hpp"

namespace tag
{
    struct Foo {};
}

struct A : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            int get()
            {
                return this->remote(tag::Foo{}).get();
            }
        };
    };

    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                return this->local(tag::Foo{}).get();
            }
        };
    };

    using Ports = ct::map<ct::pair<tag::Foo, Impl>>;
    using Services = ct::tuple<Service>;
};

struct B : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            int get()
            {
                return 42;
            }
        };
    };

    using Ports = ct::map<ct::pair<tag::Foo, Impl>>;
};

int main()
{
    cg::Host<cg::Graph<cg::Connect<A,B>>> host{};
    ASSERT(cg::Service(host) == 42);
    return result;
}
