#include <cg/service.hpp>
#include "lib/assert.hpp"

#include <iostream>
#include <set>
#include <mutex>

std::mutex m;

template<int I>
struct A : cg::Component
{
    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                std::lock_guard<std::mutex> lock(m);
                this->state.ids.insert(I);
                this->state.threads.insert(std::this_thread::get_id());
                return 0;
            }
        };
    };

    template<typename>
    struct State
    {
        State(
            std::set<int> &ids_,
            std::set<std::thread::id> &threads_
        )
        : ids(ids_)
        , threads(threads_) {}

        std::set<int> &ids;
        std::set<std::thread::id> &threads;
    };

    using Services = ct::tuple<Service>;
};

int main()
{
    std::set<int> ids;
    std::set<std::thread::id> threads;

    cg::Host<cg::Graph<A<0>, A<1>, A<2>>> host{
        cg::Args<A<0>>(ids, threads),
        cg::Args<A<1>>(ids, threads),
        cg::Args<A<2>>(ids, threads)
    };

    int rc = cg::Service(host);

    ASSERT(rc == 0);
    ASSERT(ids.size() == 3);
    ASSERT(threads.size() == 3);
    ASSERT(threads.find(std::this_thread::get_id()) != threads.end());

    return result;
}
