#include <cg/bind.hpp>
#include <cg/service.hpp>
#include "lib/assert.hpp"

#include <functional>
#include <iostream>
#include <vector>

namespace tag
{
    struct Foo {};
}

struct Server : cg::Component
{
    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                return this->remote(tag::Foo{}).value();
            }
        };
    };

    using Services = ct::tuple<Service>;
};

struct A : cg::Component
{
    struct Initializer
    {
        template<typename Base>
        struct Interface : cg::Bind<Interface, Base>
        {
            using Self = Interface<Base>;

            Interface(Base &&base) : cg::Bind<Interface, Base>{base}
            {
                this->state.value = 100;
                this->state.get = this->bind(&Self::get);
            }

            int get()
            {
                return this->state.value + 100;
            }
        };
    };

    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            int value()
            {
                return this->state.value + this->state.get();
            }
        };
    };

    template<typename>
    struct State
    {
        int value = 0;
        std::function<int()> get;
    };

    using Ports = ct::map<ct::pair<tag::Foo, Impl>>;
    using Initializers = ct::tuple<Initializer>;
};

int main()
{
    cg::Host<cg::Graph<
        cg::Connect<Server, A>
    >> host{};

    ASSERT(cg::Service(host) == 300);
    return result;
}
