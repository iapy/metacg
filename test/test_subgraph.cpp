#include <iostream>
#include <cg/service.hpp>
#include "lib/assert.hpp"

namespace tag
{
    struct Foo {};
    struct Bar {};
}

template<int I, bool OpenSecondPort = false>
struct A : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            int get()
            {
                if constexpr (Base::template has_remote<tag::Foo>)
                    return I + this->remote(tag::Foo{}).get();
                if constexpr (Base::template has_remote<tag::Bar>)
                    return this->remote(tag::Bar{}).get();
                return I;
            }
        };
    };

    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                return this->local(tag::Foo{}).get();
            }
        };
    };

    using Ports = std::conditional_t<
        OpenSecondPort,
        ct::map
        <
            ct::pair<tag::Foo, Impl>,
            ct::pair<tag::Bar, Impl>
        >,
        ct::map
        <
            ct::pair<tag::Foo, Impl>
        >
    >;

    using Services = std::conditional_t<I == 1, ct::tuple<Service>, ct::tuple<>>;
};

int main()
{
    {
        using P = cg::Pipeline<A<3>, A<4>, A<5>>;
        using G = cg::Pipeline<A<1>, A<2>, P>;

        cg::Host<cg::Graph<G>> host;
        ASSERT(cg::Service(host) == 15);
    }
    {
        using P = cg::Pipeline<A<2>, A<3>, A<4>, A<5>>;
        using G = cg::Pipeline<A<1>, P>;

        cg::Host<cg::Graph<G>> host;
        ASSERT(cg::Service(host) == 15);
    }
    {
        using P = cg::Pipeline<A<2, true>, A<3>, A<4>, A<5>>;
        using G = cg::Pipeline<A<1>, P, tag::Bar>;

        cg::Host<cg::Graph<G>> host;
        ASSERT(cg::Service(host) == 14);
    }
    {
        using P = cg::Pipeline<A<3, true>, A<4>, A<5>>;
        using G = cg::Pipeline<A<1>, A<2, true>, P, tag::Bar>;

        cg::Host<cg::Graph<G>> host;
        ASSERT(cg::Service(host) == 12);
    }

    return result;
}
