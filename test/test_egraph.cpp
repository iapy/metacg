#include <cg/connect.hpp>
#include <cg/service.hpp>
#include "lib/assert.hpp"

namespace tag
{
    struct Foo {};
}

template<std::size_t I>
struct A : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            int get()
            {
                if constexpr (Base::template has_remote<tag::Foo>)
                {
                    return I + this->remote(tag::Foo{}).get();
                }
                else
                {
                    return I;
                }
            }
        };
    };

    using Ports = ct::map<ct::pair<tag::Foo, Impl>>;
};

struct Main : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                return this->remote(tag::Foo{}).get();
            }
        };
    };

    using Services = ct::tuple<Impl>;
};


int main()
{
    using E = cg::Graph<cg::Pipeline<A<1>, A<2>, A<3>>>;
#if 0
    cg::Graph<
        E, cg::Connect<Main, A<1>>
    >::type::foo();
#endif
    cg::Host<cg::Graph<
        E, cg::Connect<Main, A<1>>
    >> host;

    ASSERT(cg::Service(host) == 6);
    return result;
}
