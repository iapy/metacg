#include <cg/connect.hpp>
#include <cg/service.hpp>
#include "lib/assert.hpp"

namespace tag
{
    struct H {};
}

namespace detail
{
    template<typename T>
    struct get_x
    {
        using type = ct::tuple<typename T::X>;
    };

    template<typename Tuple>
    using make_tuple = ct::transform_t<get_x, Tuple>;
}

struct A : cg::Component
{
    template<typename>
    struct Types
    {
        using X = int;
    };
};

struct B : cg::Component
{
    template<typename>
    struct Types
    {
        using X = float;
    };
};

struct H : cg::Component
{
    template<typename Resolver>
    struct Types
    {
        using X = detail::make_tuple<typename Resolver::template ReverseTypesTuple<tag::H>>;
    };

    struct Impl
    {
        template<typename Base>
        struct Interface
        {
        };
    };

    template<typename Types>
    struct State
    {
        State()
        {
            static_assert(std::is_same_v<typename Types::X::std_type, std::tuple<int, float>>);
        }
    };

    using Ports = ct::map<ct::pair<tag::H, Impl>>;
};

int main()
{
    cg::Host<cg::Graph<
        cg::Connect<A, H>,
        cg::Connect<B, H>
    >> host{};
    ASSERT(cg::Service(host) == 0);
    return result;
}
