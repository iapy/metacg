#include <cg/service.hpp>
#include "lib/assert.hpp"

#include <set>
#include <mutex>

namespace tag
{
    struct Service {};
}

template<int I>
struct A : cg::Component
{
    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            Interface(Base &&base) : Base(base)
            {
                ASSERT(this->state.value != 0);
                this->state.value = 0;
            }

            int run()
            {
                return this->state.value;
            }
        };
    };

    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            int get()
            {
                return this->state.value;
            }
        };
    };

    template<typename>
    struct State
    {
        int value = -1;
    };

    using Ports = ct::map<ct::pair<tag::Service, Impl>>;
    using Services = ct::tuple<Service>;
};

struct B : cg::Component
{
    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            Interface(Base &&base) : Base(base)
            {
                this->apply(tag::Service{},[](auto component){
                    ASSERT(0 == component.get());
                });
            }

            int run()
            {
                return 0;
            }
        };
    };

    using Services = ct::tuple<Service>;
};

int main()
{
    cg::Host<cg::Graph<
        cg::Connect<B, A<0>>,
        cg::Connect<B, A<1>>
    >> host{};
    ASSERT(0 == cg::Service(host));
    return result;
}
