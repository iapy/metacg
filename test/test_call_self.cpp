#include <cg/bind.hpp>
#include <cg/service.hpp>
#include "lib/assert.hpp"

#include <iostream>
#include <vector>

namespace tag
{
    struct A {};
    struct B {};
}

struct A : cg::Component
{
    struct AImpl
    {
        template<typename Base>
        struct Interface : cg::Bind<Interface, Base>
        {
            int foo()
            {
                return 1 + this->remote(tag::B{}).foo();
            }
        };
    };

    struct BImpl
    {
        template<typename Base>
        struct Interface : cg::Bind<Interface, Base>
        {
            int foo()
            {
                return 42;
            }
        };
    };

    struct Service
    {
        template<typename Base>
        struct Interface : cg::Bind<Interface, Base>
        {
            int run()
            {
                return this->local(tag::A{}).foo();
            }
        };
    };

    using Ports = ct::map<
        ct::pair<tag::A, AImpl>,
        ct::pair<tag::B, BImpl>
    >;

    using Services = ct::tuple<Service>;
};

int main()
{
    cg::Host<cg::Graph<
        cg::Connect<A, A, tag::B>
    >> host{};

    ASSERT(cg::Service(host) == 43);
    return result;
}
