#include <cg/connect.hpp>
#include <cg/service.hpp>
#include "lib/assert.hpp"

namespace tag
{
    struct Foo {};
}

template<typename T>
struct Sum;

template<typename H, typename ...T>
struct Sum<ct::tuple<H, T...>>
{
    constexpr static std::size_t value = H::value + Sum<ct::tuple<T...>>::value;
};

template<>
struct Sum<ct::tuple<>>
{
    constexpr static std::size_t value = 0;
};

struct A : cg::Component
{
    template<typename T>
    struct GetV
    {
        using type = ct::tuple<typename T::V>;
    };

    template<typename Resolver>
    struct Types
    {
        using V = ct::transform_t<GetV, typename Resolver::template TypesTuple<tag::Foo>>;
    };

    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                return Sum<typename Base::V>::value;
            }
        };
    };

    using Services = ct::tuple<Service>;
};

template<std::size_t I>
struct B : cg::Component
{
    template<typename Resolver>
    struct Types
    {
        using V = std::integral_constant<std::size_t, I>;
    };

    struct Impl
    {
        template<typename Base>
        struct Interface : Base {};
    };

    using Ports = ct::map<ct::pair<tag::Foo, Impl>>;
};

int main()
{
    cg::Host<cg::Graph<
        cg::Connect<A, cg::Group<
            B<1>, B<2>, B<3>
        >>
    >> host{};
    ASSERT(cg::Service(host) == 6);
    return result;
}
