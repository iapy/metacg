#pragma once

#include <iostream>

int result = 0;
#define ASSERT(m) if(!(m)) { std::cerr << __LINE__ << " " << #m << '\n'; result = 1; }
