#include <cg/connect.hpp>
#include <cg/service.hpp>
#include "lib/assert.hpp"

namespace tag
{
    struct Client {};
    struct Server {};
}

struct A : cg::Component
{
    template<typename Resolver>
    struct Types
    {
        using Server = typename Resolver::template Types<tag::Server>::Server;

        struct Client
        {
            std::string ss;
        };
    };

    template<typename Types>
    using State = typename Types::Server;

    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                this->state.i = 1;
                this->state.s = "ab";
                return this->remote(tag::Server{}).request(this->state);
            }
        };
    };

    struct Client
    {
        template<typename Base>
        struct Interface : Base
        {
            std::size_t get(typename Base::Client &client)
            {
                return client.ss.size();
            }
        };
    };

    using Services = ct::tuple<Service>;
    using Ports = ct::map<ct::pair<tag::Client, Client>>;
};

struct B : cg::Component
{
    template<typename Resolver>
    struct Types
    {
        struct Server : Resolver::template Types<tag::Server>::Server
        {
            int i;
        };

        struct Client : Resolver::template Types<tag::Client>::Client
        {
            int ii;
        };
    };

    struct Server
    {
        template<typename Base>
        struct Interface : Base
        {
            int request(typename Base::Server &server)
            {
                return server.i + this->remote(tag::Server{}).request(server);
            }
        };
    };

    struct Client 
    {
        template<typename Base>
        struct Interface : Base
        {
            int get(typename Base::Client &client)
            {
                return client.ii + this->remote(tag::Client{}).get(client);
            }
        };
    };

    using Ports = ct::map<
        ct::pair<tag::Server, Server>,
        ct::pair<tag::Client, Client>
    >;
};

struct C : cg::Component
{
    template<typename Resolver>
    struct Types
    {
        struct Server
        {
            std::string s;
        };

        using Client = typename Resolver::template Types<tag::Client>::Client;
    };

    struct Server
    {
        template<typename Base>
        struct Interface : Base
        {
            std::size_t request(typename Base::Server &server)
            {
                typename Base::Client client;
                client.ii = 2;
                client.ss = "cde";

                return server.s.size() + this->remote(tag::Client{}).get(client);
            }
        };
    };

    using Ports = ct::map<ct::pair<tag::Server, Server>>;
};

int main()
{
    cg::Host<cg::Graph<
        cg::Pipeline<A, B, C, tag::Server>,
        cg::Pipeline<C, B, A, tag::Client>
    >> host{};
    ASSERT(cg::Service(host) == 8);
    return result;
}
