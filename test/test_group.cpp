#include <cg/connect.hpp>
#include <cg/service.hpp>
#include "lib/assert.hpp"

namespace tag
{
    struct Foo {};
}

template<int I>
struct A : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            int get()
            {
                return I;
            }
        };
    };

    using Ports = ct::map<ct::pair<tag::Foo, Impl>>;
};

struct B : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            int get()
            {
                int result = 0;
                this->apply(tag::Foo{}, [&result](auto remote) mutable {
                    result += remote.get();
                });
                return result;
            }
        };
    };

    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                return this->local(tag::Foo{}).get();
            }
        };
    };
        
    using Ports = ct::map<ct::pair<tag::Foo, Impl>>;
    using Services = ct::tuple<Service>;
};

int main()
{
    {
        cg::Host<cg::Graph<
            cg::Connect<B, cg::Group<
                A<1>,
                A<2>,
                A<3>,
                A<4>
            >, tag::Foo>
        >> host{};
        ASSERT(cg::Service(host) == 10);
    }
    {
        cg::Host<cg::Graph<
            cg::Connect<B, cg::Group<
                A<1>,
                A<2>,
                A<3>,
                A<4>
            >>
        >> host{};
        ASSERT(cg::Service(host) == 10);
    }
    {
        cg::Host<cg::Graph<
            cg::Connect<B, cg::Group<A<1>>>
        >> host{};
        ASSERT(cg::Service(host) == 1);
    }
    return result;
}
