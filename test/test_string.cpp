#include <ct/string.hpp>
#include "lib/assert.hpp"

struct Foo
{
    static constexpr auto name = CT_STRING("foo");
};

struct Bar
{
    static constexpr auto name = CT_STRING("bar"); 
};

struct FooBar
{
    static constexpr auto name = Foo::name + Bar::name;
};

int main()
{
    using namespace ct::string_literals;
    static_assert(Foo::name == "foo"_cts);
    static_assert(Bar::name == "bar"_cts);
    static_assert(FooBar::name == "foobar"_cts);
    static_assert(", "_cts.join(Foo::name, Bar::name) == "foo, bar"_cts);
    return 0;
}
