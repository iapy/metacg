#include <cg/connect.hpp>
#include <cg/service.hpp>
#include "lib/assert.hpp"

namespace tag
{
    struct Foo {};
}

struct A : cg::Component
{
    template<typename Resolver>
    struct Types
    {
        static constexpr std::size_t v = 1 + Resolver::template Types<tag::Foo>::v;
    };

    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                return Base::v;
            }
        };
    };

    using Services = ct::tuple<Service>;
};

struct B : cg::Component
{
    template<typename Resolver>
    struct Types
    {
        static constexpr std::size_t v = 2 + Resolver::template Types<tag::Foo>::v;
    };

    struct Impl
    {
        template<typename Base>
        struct Interface : Base {};
    };

    using Ports = ct::map<ct::pair<tag::Foo, Impl>>;
};

struct C : cg::Component
{
    template<typename Resolver>
    struct Types
    {
        static constexpr std::size_t v = 3;
    };

    struct Impl
    {
        template<typename Base>
        struct Interface : Base {};
    };

    using Ports = ct::map<ct::pair<tag::Foo, Impl>>;
};

int main()
{
    cg::Host<cg::Graph<
        cg::Pipeline<A, B, C>
    >> host{};
    ASSERT(cg::Service(host) == 6);
    return result;
}
