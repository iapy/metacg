#include <cg/bind.hpp>
#include <cg/service.hpp>
#include "lib/assert.hpp"

#include <iostream>
#include <vector>

namespace tag
{
    struct Foo {};
}

struct Server : cg::Component
{
    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                this->apply(tag::Foo{}, [this](auto interface){
                    interface.routes(this->state);
                });

                int res = 0;
                for(auto &p : this->state.endpoints)
                {
                    res += p();
                }

                return res;
            }
        };
    };

    template<typename>
    struct State
    {
        void add(std::function<int()> f)
        {
            endpoints.push_back(f);
        }
        std::vector<std::function<int()>> endpoints;
    };

    using Services = ct::tuple<Service>;
};

template<int I>
struct A : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : cg::Bind<Interface, Base>
        {
            template<typename State>
            void routes(State &state)
            {
                using Self = Interface<Base>;
                state.add(this->bind(&Self::foo));
            }

            int foo()
            {
                return I;
            }
        };
    };

    using Ports = ct::map<ct::pair<tag::Foo, Impl>>;
};

int main()
{
    cg::Host<cg::Graph<
        cg::Connect<Server, A<5>>,
        cg::Connect<Server, A<3>>
    >> host{};

    ASSERT(cg::Service(host) == 8);
    return result;
}
