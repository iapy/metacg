# Abstract

The **MetaCG** library, which stands for **META Component Graph**, is a header-only library that provides zero-cost abstractions for developing (meta)components. These components can be combined statically into a dependency graph that propagates type traits. The library defines two namespaces:

1. `ct::` namespace contains compile-time utilities such as tuples, maps, graphs, and functional algorithms.
2. `cg::`here, you’ll find various types related to component graphs.

## Component

The fundamental class for every component is `cg::Component`. This class defines the complete interface that every component should implement:

```c++
strut Component
{
    template<typename>
    strut Types {};

    template<typename>
    strut State {};

    using Ports = ct::map<>;
    using Services = ct::tuple<>;
    using Initializers = ct::tuple<>;
};
```

By creating subclasses of `cg::Component`, developers can override each of the types defined in the structure above to implement application logic.

Let’s break down the purpose of each inner type in the `cg::Component` class:

1. **`Types`** contains type definitions that connected components may use as type traits.
2. **`State`** represents the internal state of the component. It encapsulates data and information specific to the component’s functionality.
3. **`Ports`** define the interfaces that the component exposes to connected components. These interfaces allow communication between different components.
4. **`Services`** represent threads or background tasks implemented by the component.
5. **`Initializers`** are additional logic executed during the construction of the component’s state. They allow you to customise the initialisation process beyond what the `State` constructor provides. They may access connected components during initialisation. The framework ensures that connected components are initialised at the time initialisers are executed.

### Interfaces

```c++
#include <cg/component.hpp>

struct Demo : cg::Component
{
    struct I
    {
        template<typename Base>
        struct Interface : Base
        {
            // interface methods
        };
    };
};
```

The provided code snippet defines a component named `Demo` along with an associated interface called `I`. To declare an interface, developers should follow the template shown above, adjusting the interface name (in this case, `I`) and implementing its methods.

### Services

In a `metacg`-based application, it is essential to have at least one component that defines a service. These services are mapped to native operating system threads during startup. Each service is an interface with a required method `int run()`. This method serves as the entry point for the spawned thread:

```c++
#include <cg/component.hpp>
#include <iostream>

struct Main : cg::Component
{
     struct Thread
     {
         template<typename Base>
         struct Interface : Base
         {
             int run()
             {
                  std::cout << "Hello World!\n";
                  return 0;
             }
         };
     };

     using Services = ct::tuple<Thread>;
};
```

The code snippet represents a basic _Hello World_ example using the `metacg` component model.

1. **`struct Thread::Interface`**
    - The `struct Thread` defines an interface, which we’ve previously discussed.
2. **`Services` typedef:**
    - The `Services` typedef serves as a directive to the `metacg` framework.
    - It specifies which interfaces within the component should be treated as services.
    - These services correspond to threads that the framework will spawn during execution.

To observe the printed _Hello World!_ message, you’ll need to feed the defined component to the `metacg`framework:

```c++
#include <cg/service.hpp>
#include <cg/graph.hpp>
#include <cg/host.hpp>

int main()
{
    using G = cg::Graph<Main>;
    auto host = cg::Host<G>{};
    return cg::Service(host);
}
```

In the given code snippet:

1. **`G`**: Represents a graph of components, with a single node named `Main`.
2. **`host`**: Serves as the host for initialised graph nodes (components).
3. The invocation of `cg::Service`:
	- Spawns all the threads (services defined by components).
	- Waits for their completion.
    - `cg::Service` returns 0 if all run methods return 0 and 1 otherwise.

The component may spawn as may threads as needed:


```c++
#include <cg/component.hpp>
#include <iostream>
#include <mutex>

std::mutex mutex;

struct Main : cg::Component
{
     template<std::size_t N>
     struct Thread
     {
         template<typename Base>
         struct Interface : Base
         {
             int run()
             {
                  std::unique_lock lock{mutex};
                  std::cout << "Hello from thread " << N << "\n";
                  return 0;
             }
         };
     };

     using Services = ct::tuple<Thread<1>, Thread<2>>;
};
```

To demonstrate how `metacg` thread model works, we need to modify the snippet above:

```c++
#include <cg/component.hpp>
#include <iostream>
#include <mutex>

std::mutex mutex;

struct Main : cg::Component
{
     template<std::size_t N>
     struct Thread
     {
         template<typename Base>
         struct Interface : Base
         {
             int run()
             {
                  std::unique_lock lock{mutex};
                  std::cout << "Thread " << N << " (thread id = " << std::this_thread::get_id() << ")\n";
                  return 0;
             }
         };
     };

     using Services = ct::tuple<Thread<1>, Thread<2>>;
};

#include <cg/service.hpp>
#include <cg/graph.hpp>
#include <cg/host.hpp>

int main()
{
    std::cout << "Main thread " << std::this_thread::get_id() << "\n";
    using G = cg::Graph<Main>;
    auto host = cg::Host<G>{};
    return cg::Service(host);
}
```

On my system, the output appears as follows:

```
Main thread 0x109278600
Thread 1 (thread id = 0x109278600)
Thread 2 (thread id = 0x700004058000)
```

In the context of the `metacg`, the total number of threads is equal to the sum of the sizes of the `Services` tuple for all components. To illustrate how multiple components fit into this threading model, let's consider two components.

```c++
#include <cg/component.hpp>
#include <iostream>
#include <mutex>

std::mutex mutex;

template<std::size_t C>
struct Main : cg::Component
{
     template<std::size_t N>
     struct Thread
     {
         template<typename Base>
         struct Interface : Base
         {
             int run()
             {
                  std::unique_lock lock{mutex};
                  std::cout << "Thread " << N << " of component " << C << " (thread id = " << std::this_thread::get_id() << ")\n";
                  return 0;
             }
         };
     };

     using Services = ct::tuple<Thread<1>, Thread<2>>;
};

#include <cg/service.hpp>
#include <cg/graph.hpp>
#include <cg/host.hpp>

int main()
{
    std::cout << "Main thread " << std::this_thread::get_id() << "\n";
    using G = cg::Graph<Main<1>, Main<2>>;
    auto host = cg::Host<G>{};
    return cg::Service(host);
}
```

The output is:

```
Main thread 0x117f3f600
Thread 1 of component 2 (thread id = 0x117f3f600)
Thread 2 of component 2 (thread id = 0x70000b203000)
Thread 1 of component 1 (thread id = 0x70000b286000)
Thread 2 of component 1 (thread id = 0x70000b309000)
```

In this example, making `Main` templated is necessary because for `cg::Graph<>`, different types represent different nodes in the graph. Even if the same type is passed multiple times as `cg::Graph<>`'s template argument (e.g., `cg::Graph<Main, Main>`, `cg::Graph<Main, Main, Main>`), all of these are equivalent to `cg::Graph<Main>`. However, in real-world scenarios, you’ll likely have different components with distinct names.

If we replace the line 

`using G = cg::Graph<Main<1>, Main<2>>` 

with 

`using G = cg::Graph<Main<1>, Main<1>>`

there would be only two threads of one component.

### State

The `State` definition within a component serves as an analogy to the `private:` fields in a standard C++ class:

```c++
#include <cg/component.hpp>
#include <iostream>

struct Main : cg::Component
{
     struct Thread
     {
         template<typename Base>
         struct Interface : Base
         {
             int run()
             {
                  std::cout << "Hello, " << this->state.name << "\n";
                  return 0;
             }
         };
     };

     template<typename>
     struct State
     {
          std::string const name;
     };

     using Services = ct::tuple<Thread>;
};

#include <cg/service.hpp>
#include <cg/graph.hpp>
#include <cg/host.hpp>

int main(int argc, char **argv)
{
    using G = cg::Graph<Main>;
    auto host = cg::Host<G>{
        cg::Args<Main>(argv[1])
    };
    return cg::Service(host);
}
```

* To access the component’s `State`, we use the `this->state` reference from within the component’s `Interface`, which is defined in the `Base` class.
* The `State` of each component is constructed once within the `cg::Host` constructor, and the `cg::Args<Main>` function is used to pass arguments to the `State`’s constructor for the component `Main`.
* The `State` **must be** movable, meaning that all non-movable types (such as `std::mutex`) are supposed to be wrapped with `std::unique_ptr`.
* `cg::Args<>` supports perfect forwarding.

### Ports

**Ports** serve as a mechanism for components to expose their interfaces to other components. Specifically, they are represented as a `ct::map` (compile-time map) of tags (often empty structures) associated with the interfaces provided by the component.

```c++
#include <cg/component.hpp>
#include <iostream>
#include <vector>

namespace tag
{
    struct Consumer {};
}

struct Consumer : cg::Component
{
    struct Impl
    {
         template<typename Base>
         struct Interface : Base
         {
             void consume(int value)
             {
                 this->state.values.push_back(value);
             }

             std::vector<int> const &consumed()
             {
                 return this->state.values;
             }
         };
    };

    template<typename>
    struct State
    {
        std::vector<int> values;
    };

    using Ports = ct::map
    <
        ct::pair<tag::Consumer, Impl>
    >;
};
```

In the provided code snippet, the `Consumer` component exposes an interface named `Impl`. This interface can be accessed by other connected components using the tag `tag::Consumer`:

```c++
struct Producer : cg::Component
{
     struct Thread
     {
         template<typename Base>
         struct Interface : Base
         {
             int run()
             {
                  auto consumer = this->remote(tag::Consumer{});
                  consumer.consume(1);
                  consumer.consume(2);
                  consumer.consume(3);
                  for(auto const &value : consumer.consumed())
                  {
                      std::cout << "Consumed " << value << "\n";
                  }
                  return 0;
             }
         };
     };

     using Services = ct::tuple<Thread>;
};
```

In the given scenario, the `Producer` component acquires the interface associated with the `tag::Consumer`from the connected component. To observe how this interaction functions, it is necessary to establish a connection between the `Producer` and `Consumer` components using the `tag::Consumer` in the graph:

```c++
#include <cg/service.hpp>
#include <cg/graph.hpp>
#include <cg/host.hpp>

int main(int argc, char **argv)
{
    using G = cg::Graph<
        Producer,
        Consumer,
        cg::Connect<Producer, Consumer, tag::Consumer>
    >;
    auto host = cg::Host<G>{};
    return cg::Service(host);
}
```

The `cg::Connect` establishes connections between components. Its first template argument represents the `Producer` component, which can utilise the interface provided by its second argument, the `Consumer`. This interface is identified by the third argument, `tag::Consumer`.

The `cg::Graph` is intelligent enough to infer the list of nodes from the provided list of edges. As a result, the graph `G` can be redefined accordingly:

```c++
    using G = cg::Graph<
        cg::Connect<Producer, Consumer, tag::Consumer>
    >;
```

Or even simpler because `Consumer` has only one interface (the following would be a compile time error if `Consumer` had multiple interfaces exposed)

```c++
    using G = cg::Graph<
        cg::Connect<Producer, Consumer>
    >;
```

It’s feasible to refactor the producer-consumer code mentioned earlier, combining both interfaces into a single component:

```c++
#include <cg/component.hpp>
#include <iostream>
#include <vector>

namespace tag
{
    struct Consumer {};
}

struct Main : cg::Component
{
    struct Consumer
    {
         template<typename Base>
         struct Interface : Base
         {
             void consume(int value)
             {
                 this->state.values.push_back(value);
             }

             std::vector<int> const &consumed()
             {
                 return this->state.values;
             }
         };
    };

    struct Producer
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                 auto consumer = this->local(tag::Consumer{});
                 consumer.consume(1);
                 consumer.consume(2);
                 consumer.consume(3);
                 for(auto const &value : consumer.consumed())
                 {
                     std::cout << "Consumed " << value << "\n";
                 }
                 return 0;
            }
        };
    };

    template<typename>
    struct State
    {
        std::vector<int> values;
    };

    using Ports = ct::map
    <
        ct::pair<tag::Consumer, Consumer>
    >;
    using Services = ct::tuple<Producer>;
};

#include <cg/service.hpp>
#include <cg/graph.hpp>
#include <cg/host.hpp>

int main(int argc, char **argv)
{
    using G = cg::Graph<Main>;
    auto host = cg::Host<G>{};
    return cg::Service(host);
}
```

In contrast to the two-components code, the difference lies in using `this->local(tag::Consumer{})`instead of `this->remote(tag::Consumer{})`. The former retrieves an instance of the interface tagged as `tag::Consumer` from the current component itself, rather than from a connected component. All local interfaces within the component are mutually accessible without requiring additional graph edges.

The objects returned by the `this->local(...)` and `this->remote(...)` calls are instances of a lightweight class. This class is fast-constructible and fast-copyable, containing the interface implementation and a private reference to the state of the component that owns the interface. For example, in the first case, it would be the `Consumer` component, and in the second case, it would be the `Main` component.

### Types

`Types` are type traits of a component. Let's get back to our consumer-producer example and introduce a type consumer can consume:

```c++
template<typename T>
struct Consumer : cg::Component
{
    template<typename Resolver>
    struct Types
    {
        using type = T;
    };

    struct Impl
    {
         template<typename Base>
         struct Interface : Base
         {
             void consume(typename Base::type &&value)
             {
                 this->state.values.emplace_back(value);
             }

             std::vector<typename Base::type> const &consumed()
             {
                 return this->state.values;
             }
         };
    };

    template<typename Types>
    struct State
    {
        std::vector<typename Types::type> values;
    };

    using Ports = ct::map
    <
        ct::pair<tag::Consumer, Impl>
    >;
};
```

- The `Resolver` template argument within the `Types` class serves as a specialized component for handling type resolution.
- The `Types` template argument for the `State` class is an instance of `Types<>` with the appropriate resolver passed.
- Each interface’s `Base` class is derived from `Types<>` and includes the correct resolver as an argument. Consequently, it encompasses all the types defined in `Types`.

Let's modify the `Producer` such that it can work with consumers consuming diffent types:

```c++
struct Producer : cg::Component
{
    template<typename Resolver>
    struct Types
    {
        using type = typename Resolver::template Types<tag::Consumer>::type;
    };

    struct Thread
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                 auto consumer = this->remote(tag::Consumer{});
                 consumer.consume(typename Base::type{});
                 for(auto const &value : consumer.consumed())
                 {
                     std::cout << "Consumed " << value << "\n";
                 }
                 return 0;
            }
        };
    };
};
```

* `Resolver::template Types<tag::Consumer>` is a `Types<>` of component connected by `tag::Consumer` with correct `Resolver`.

Now our producer can work with different consumers:

```c++
int main(int argc, char **argv)
{
    using G = cg::Graph<cg::Connect<Producer, Consumer<std::string>>>;
    auto host = cg::Host<G>{};
    return cg::Service(host);
}
```

```
int main(int argc, char **argv)
{
    using G = cg::Graph<cg::Connect<Producer, Consumer<int>>>;
    auto host = cg::Host<G>{};
    return cg::Service(host);
}
```

The `Resolver` is utilized to propagate type traits throughout the graph. For instance, if we have a producer tagged as an interface (e.g., `tag::Producer`), and another component uses it, that component would be able to access the types defined within the `Producer` using the same technique:

```c++
struct User : cg::Component
{
    template<typename Resolver>
    struct Types
    {
        using type = typename Resolver::template Types<tag::Producer>::type;
    };
};
```

### any-to-any connections

Let’s explore creating a more generic producer and consumer logic within the context of `cg::Graph<>`. As mentioned, `cg::Graph<>` is a generic graph that allows connecting multiple components through ports. To achieve this, we’ll focus on designing a system where different consumers can consume instances of various types produced by a common producer:

```c++
#include <cg/component.hpp>
#include <cg/service.hpp>
#include <cg/graph.hpp>
#include <cg/host.hpp>
#include <iostream>

namespace tag
{
    struct Consumer {};
}

template<std::size_t I, typename T>
struct Consumer : cg::Component
{
    template<typename Resolver>
    struct Types
    {
        using type = T;
    };

    struct Impl
    {
         template<typename Base>
         struct Interface : Base
         {
             void consume(typename Base::type &&value)
             {
                 std::cout << "Consumer " << I << " consumed " << value << "\n";
             }

             template<typename X>
             static constexpr bool can_consume = std::is_same_v<X, typename Base::type>;
         };
    };

    using Ports = ct::map
    <
        ct::pair<tag::Consumer, Impl>
    >;
};

struct Producer : cg::Component
{
    struct Thread
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                 produce(1);
                 produce(2);
                 produce(std::string{"foo"});
                 produce(std::string{"bar"});
                 return 0;
            }
        private:
            template<typename T>
            void produce(T&& value)
            {
                this->apply(tag::Consumer{}, [&](auto interface) {
                    if constexpr (decltype(interface)::template can_consume<T>)
                        interface.consume(std::forward<T>(value));
                });
             }
        };
    };

    using Services = ct::tuple<Thread>;
};

int main()
{
    using G = cg::Graph<
        cg::Connect<Producer, Consumer<1, int>>,
        cg::Connect<Producer, Consumer<2, std::string>>
    >;
    auto host = cg::Host<G>{};
    return cg::Service(host);
}
```

The output of the code is:
```
Consumer 1 consumed 1
Consumer 1 consumed 2
Consumer 2 consumed foo
Consumer 2 consumed bar
```

Instead of using `this->remote(tag::Consumer{})`, we should use `this->apply(tag::Consumer{}, [](auto){...})`to iterate (at compile time) over the connected components. Using `this->remote` in this case would result in a compile-time error.

The `std::size_t I` template argument has no functional significance but is used for printing which component consumed which value.

There's a shorthand for connecting multiple components by one tag:

```c++
    using G = cg::Graph<
        cg::Connect<Producer, cg::Group<
            Consumer<1, int>,
            Consumer<2, std::string>
        >>
    >;
```

Connecting many components in an opposite order (i.e. many producers - one consumer) is obvious and requires no additional explanation.

### one-to-many connection: Types

Types of all connected components are the tuple and can be obtained using the following trick:

```c++
template<typename Resolver>
struct Types
{
    // this is an equivalent of typename Resolver::Types<tag::Consumer>
    using Tuple = typename Resolver::template TypesTuple<tag::Consumer>;

    template<typename T>
    struct get_type
    {
        using type = ct::tuple<typename T::type>;
    };

    // this is an equivalent of typename Resolver::template Types<tag::Consumer>::type;
    using type = ct::transform_t<get_type, Tuple>;

    // For the case of two consumers with int and std::string
    static_assert(std::is_same_v<type, ct::tuple<int, std::string>>);
};
```

### Initializers

`Initializers` are the interfaces that are initialised only once **before** any of `metacg` threads are spawned by and **after** all the component's states are initialised. They may be used in a rare case when additional initialisation of a component that depends on connected components is required. Their syntax is the following:

```c++
struct Demo : cg::Component
{
    struct Initializer
    {
        template<typename Base>
        struct Interface : Base
        {
            Interface(Base &&base) : Base{std::forward<Base>(base)}
            {
                this->apply(tag::Consumer{}, [](auto interface) {
                    // assuming consumer has a persistent queue somewhere in a database
                    interface.cleanup();
                });
            }
        };
    };

    using Initializers = ct::tuple<Initializer>;
};
```

The same constructor can be added to a `Service` to initialise the thread. Using such a constructor with normal (non-service and non-initialiser) interfaces is not recommended as the interface is constructed every time `this->local` or `this->remote` is called unless you know what you're doing.

### Interface extensions

One can create a reusable add-on for an interface. An instance of such an add-on is `cg::Bind`:

```c++
template<typename Base>
struct Interface : cg::Bind<Interface, Base>
{
    template<typename T>
    void init(T &dispatcher)
    {
        using Self = Interface<Base>;
        dispatcher.register_handler("/", this->bind(&Self::home);
        dispatcher.register_handler("/login", this->bind(&Self::login);
    }

private:
    auto home() { ... }
    auto login() { ... }
};
```

`cg::Bind` enhances the capabilities of `Base` by adding an extra method `bind` to the interface (in addition to `local`, `remote`, `apply`). This `bind` method transforms the interface method into a lambda function, capturing a reference to the component’s `State` and an instance of the interface.

To create your own custom extension, the following template needs to be implemented:

```c++
template<typename Base>
struct WithLogger : Base
{
    auto info(std::string const &message)
    {
        if constexpr (Base::has_remote<tag::Logger>)
            this->remote(tag::Logger{}).info(message);
    }
};

// and in some interface

template<typename Base>
struct Interface : WithLogger<Base>
{
    int run()
    {
        this->info("Starting...");
        return 0;
    }
};
```

The `has_remote` template is a constexpr variable of a `Base` for an interface, which verifies if the component is connected to something that offers that interface.

While it’s not always necessary to pass `Interface` to an extension, if your extension is expanding upon `cg::Bind` or any other extension that necessitates `Interface`, you’ll need to provide it:

```c++
template<template <typename> Interface, typename Base>
struct WithLogger : cg::Bind<Interface, Base>
{
    void info(std::string const &message)
    {
        if constexpr (Base::has_remote<tag::Logger>)
            this->remote(tag::Logger{}).info(message);
    }
};

// and in some interface

template<typename Base>
struct Interface : WithLogger<Interface, Base>
{
    int run()
    {
        this->info("Starting...");
        return 0;
    }
};
```

### Libraries

Several libraries are available that are built on top of `metacg`:

* [Reading states of components from JSON file](https://bitbucket.org/iapy/config)
* [Messaging between services](https://bitbucket.org/iapy/msgbus)
* [Logging framework](https://bitbucket.org/iapy/logger)
* [In-memory database](https://bitbucket.org/iapy/jsondb)
* [WEB framework](https://bitbucket.org/iapy/weblib)
