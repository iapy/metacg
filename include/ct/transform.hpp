#pragma once
#include <ct/tuple.hpp>

namespace ct
{
    template<template <class...> typename F, typename>
    struct transform;

    template<template <class...> typename F, typename Head, typename ...Tail>
    struct transform<F, tuple<Head, Tail...>>
    {
        using type = join_t<typename F<Head>::type, typename transform<F, tuple<Tail...>>::type>;
    };

    template<template <class...> typename F>
    struct transform<F, tuple<>>
    {
        using type = tuple<>;
    };

    template<template <class...> typename F, typename T>
    using transform_t = typename transform<F, T>::type;

#ifdef METACG_ENABLE_STATIC_TESTS            
    namespace test
    {
        namespace detail_transform_hpp
        {
            template<typename T>
            struct identity
            {
                using type = tuple<T>;
            };

            template<typename T>
            struct duplicate
            {
                using type = tuple<T, T>;
            };
        }

        static_assert(std::is_same_v<
            tuple<std::uint16_t, std::uint32_t, std::uint64_t>,
            transform_t
            <
                detail_transform_hpp::identity,
                tuple<std::uint16_t, std::uint32_t, std::uint64_t>
            >
        >);

        static_assert(std::is_same_v<
            tuple<std::uint16_t, std::uint16_t, std::uint32_t, std::uint32_t, std::uint64_t, std::uint64_t>,
            transform_t
            <
                detail_transform_hpp::duplicate,
                tuple<std::uint16_t, std::uint32_t, std::uint64_t>
            >
        >);
    }
#endif    

    template<typename>
    struct flatten;

    template<typename ...T>
    struct flatten<tuple<T...>>
    {
        using type = transform_t<identity, tuple<T...>>;
    };

    template<typename T>
    using flatten_t = typename flatten<T>::type;

#ifdef METACG_ENABLE_STATIC_TESTS            
    namespace test
    {
        static_assert(std::is_same_v<
            flatten_t<tuple<tuple<std::uint16_t, std::uint32_t>, tuple<std::uint32_t, std::uint64_t>>>,
            tuple<std::uint16_t, std::uint32_t, std::uint32_t, std::uint64_t>
        >);
    }
#endif    
}

