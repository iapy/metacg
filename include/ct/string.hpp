#pragma once
#include <string>
#include <cstring>
#include <iostream>

namespace ct {

template<char ...ch>
struct string
{
    constexpr static char data[sizeof...(ch) + 1] = {ch..., 0};

    template<char ...dh>
    constexpr string<dh...> join(string<dh...>)
    {
        return string<dh...>{};
    }

    template<typename Head, typename ...Tail>
    constexpr auto join(Head head, Tail ...tail)
    {
        return head + string<ch...>{} + string<ch...>{}.join(tail...);
    }

    constexpr operator std::string () const
    {
        return std::string(&data[0], sizeof...(ch));
    }

    constexpr operator std::string_view () const
    {
        return std::string_view(&data[0], sizeof...(ch));
    }
};

template<char ...ch>
std::string operator + (std::string const &a, string<ch...> b)
{
    return a + static_cast<std::string>(b);
}

template<char ...ch>
std::string operator + (string<ch...> a, std::string const &b)
{
    return static_cast<std::string>(a) + b;
}

template<char ...ch, char ...dh>
constexpr string<ch..., dh...> operator + (string<ch...>, string<dh...>)
{
    return string<ch..., dh...>{};
}

template<char ...ch, char ...dh>
constexpr bool operator == (string<ch...>, string<dh...>)
{
    return false;
}

template<char ...ch>
constexpr bool operator == (string<ch...>, string<ch...>)
{
    return true;
}

template<char ...ch, char ...dh>
constexpr bool operator != (string<ch...> a, string<dh...> b)
{
    return !(a == b);
}

template<char ...ch>
std::ostream &operator << (std::ostream &stream, string<ch...> const &sv)
{
    return stream << sv.data;
}

namespace string_literals {

template<typename T, T ...ch>
constexpr string<ch...> operator ""_cts ()
{
    static_assert(std::is_same_v<T, char>);
    return string<ch...>{};
}

} // namespace string_literals

#define CT_STRING(v) []{ using namespace ct::string_literals; return v##_cts; }();

} // namespace ct
