#pragma once

#define CT_TYPE(name) template<typename ...Tail> struct name

#define CT_CONCEPT(name, ...)                    \
    auto name##_impl(int) -> decltype (          \
        __VA_ARGS__, std::true_type{}            \
    );                                           \
    template<class T>                            \
    std::false_type name##_impl(...);            \
    template<class T>                            \
    using name = decltype(name##_impl<T>(0));    \
    template<class T>                            \
    constexpr bool name##_v = name<T>::value;

#include <utility>

namespace ct
{
    CT_TYPE(join);
    CT_TYPE(unique);
    CT_TYPE(reverse);
    CT_TYPE(count);
    CT_TYPE(get);
    CT_TYPE(insert);
    CT_TYPE(append);
    CT_TYPE(prepend);

    template<std::size_t I>
    using index = std::integral_constant<std::size_t, I>;

    template<typename ...Tail>
    using get_t = typename get<Tail...>::type;

    template<typename ...Tail>
    using insert_t = typename insert<Tail...>::type;

    template<typename ...Tail>
    using append_t = typename append<Tail...>::type;

    template<typename ...Tail>
    using prepend_t = typename prepend<Tail...>::type;

    template<typename T>
    struct identity
    {
        using type = T;
    };

    template<typename T>
    constexpr T sum(std::integer_sequence<T>)
    {
        return 0;
    }

    template<typename T, T head, T ...tail>
    constexpr T sum(std::integer_sequence<T, head, tail...>)
    {
        return static_cast<std::size_t>(head) + sum(std::integer_sequence<T, tail...>{});
    }
}

