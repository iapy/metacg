#pragma once
#include <ct/map.hpp>
#include <ct/tuple.hpp>

namespace ct
{
    template<template <typename...> typename F, typename>
    struct apply;

    template<template <typename...> typename F, typename ...T>
    struct apply<F, tuple<T...>>
    {
        using type = F<T...>;
    };

    template<template <typename...> typename F, typename T>
    using apply_t = typename apply<F, T>::type;

#ifdef METACG_ENABLE_STATIC_TESTS            
    namespace test
    {
        static_assert(std::is_same_v<
            map<pair<std::int16_t, std::int32_t>, pair<std::int32_t, std::int64_t>>,
            apply_t<map, tuple<pair<std::int16_t, std::int32_t>, pair<std::int32_t, std::int64_t>>>
        >);
    }
#endif    
}
