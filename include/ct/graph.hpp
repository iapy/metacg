#pragma once
#include <ct/map.hpp>
#include <ct/filter.hpp>
#include <ct/transform.hpp>

namespace ct
{
    template<class A_, class B_, class D_ = void>
    struct edge
    {
        using A    = A_;
        using B    = B_;
        using Data = D_;
    };

    CT_TYPE(graph);

    template<typename A, typename B, typename D, typename ...Tail>
    struct graph<edge<A, B, D>, Tail...>
    {
        using edges = insert_t
        <
            pair
            <
                pair<A, B>,
                prepend_t<D, get_t<pair<A, B>, typename graph<Tail...>::edges, tuple<>>>
            >,
            typename graph<Tail...>::edges
        >;
        using vertices = unique_t<join_t<tuple<A, B>, typename graph<Tail...>::vertices>>;
    };

    template<typename A, typename ...Tail>
    struct graph<A, Tail...>
    {
        using edges = typename graph<Tail...>::edges;
        using vertices = unique_t<prepend_t<A, typename graph<Tail...>::vertices>>;
    };

    template<>
    struct graph<>
    {
        using edges = map<>;
        using vertices = tuple<>;
    };

    template<typename X, typename Y, typename ...Tail>
    struct get<pair<X, Y>, graph<Tail...>>
    {
        using type = get_t<pair<X, Y>, typename graph<Tail...>::edges>;
    };

#ifdef METACG_ENABLE_STATIC_TESTS            
    namespace test
    {
        static_assert(std::is_same_v<
            graph<std::uint16_t, std::uint32_t>::vertices,
            tuple<std::uint16_t, std::uint32_t>
        >);

        static_assert(std::is_same_v<
            graph<std::uint16_t, std::uint32_t>::edges,
            map<>
        >);

        static_assert(std::is_same_v<
            graph
            <
                edge<std::uint16_t, std::uint32_t, index<0>>,
                edge<std::uint32_t, std::uint64_t, index<0>>
            >::vertices,
            tuple<std::uint16_t, std::uint32_t, std::uint64_t>
        >);

        static_assert(std::is_same_v<
            graph
            <
                edge<std::uint16_t, std::uint32_t, index<0>>,
                edge<std::uint32_t, std::uint64_t, index<0>>
            >::edges,
            map
            <
                pair<pair<std::uint16_t, std::uint32_t>, tuple<index<0>>>,
                pair<pair<std::uint32_t, std::uint64_t>, tuple<index<0>>>
            >
        >);

        static_assert(std::is_same_v<
            graph
            <
                edge<std::uint16_t, std::uint32_t, index<0>>,
                edge<std::uint16_t, std::uint32_t, index<1>>,
                edge<std::uint32_t, std::uint64_t, index<0>>
            >::edges,
            map
            <
                pair<pair<std::uint16_t, std::uint32_t>, tuple<index<0>, index<1>>>,
                pair<pair<std::uint32_t, std::uint64_t>, tuple<index<0>>>
            >
        >);

        static_assert(is_same_v<
            graph
            <
                std::uint16_t,
                edge<std::uint32_t, std::uint64_t, index<0>>
            >::edges,
            map
            <
                pair<pair<std::uint32_t, std::uint64_t>, tuple<index<0>>>
            >
        >);

        static_assert(is_same_v<
            get_t
            <
                pair<std::uint16_t, std::uint32_t>,
                graph
                <
                    edge<std::uint16_t, std::uint32_t, index<0>>,
                    edge<std::uint32_t, std::uint64_t, index<1>>
                >
            >,
            tuple<index<0>>
        >);

        static_assert(is_same_v<
            get_t
            <
                pair<std::uint32_t, std::uint64_t>,
                graph
                <
                    edge<std::uint16_t, std::uint32_t, index<0>>,
                    edge<std::uint32_t, std::uint64_t, index<1>>
                >
            >,
            tuple<index<1>>
        >);
    }
#endif    

    namespace detail
    {
        CT_TYPE(find_paths);
        CT_TYPE(extend_paths);

        template<typename T>
        struct tuple_tuple
        {
            using type = tuple<tuple<T>>;
        };

        template<typename Path>
        struct get_last
        {
            using type_ = get_t<index<Path::size - 1>, Path>;
            using type = tuple<type_>;
        };

        template<typename Path>
        struct get_first
        {
            using type = tuple<tuple<get_t<index<0>, Path>>>;
        };

        template<typename Path>
        struct tuple_append
        {
            template<typename T>
            struct filter
            {
                using type = tuple<append_t<Path, get_t<index<1>, T>>>;
            };
        };

        template<typename G>
        struct append
        {
            template<typename V>
            struct outgoing
            {
                template<typename Edge>
                struct filter
                {
                    static constexpr bool value = std::is_same_v<get_t<index<0>, Edge>, V>;
                };
            };

            template<typename Path>
            struct one
            {
                using type = 
                    transform_t<
                        tuple_append<Path>::template filter,
                        filter_t<
                            outgoing<
                                typename get_last<Path>::type_ 
                            >::template filter,
                            typename G::edges::keys
                        >
                    >;
            };
        };

        template<typename Visited>
        struct remove_visited
        {
            template<typename T>
            struct filter
            {
                static constexpr bool value = (0 == count_v<typename get_last<T>::type_, Visited>);
            };
        };

        template<typename Interesting>
        struct interesting_paths
        {
            template<typename T>
            struct filter
            {
                static constexpr bool value = (
                    1 != T::size &&
                    0 != count_v<get_t<index<0>, T>, Interesting> &&
                    0 != count_v<get_t<index<T::size - 1>, T>, Interesting>
                );
            };
        };

        template<typename Tuple>
        struct first_and_last
        {
            using type = tuple<pair<get_t<index<0>, Tuple>, get_t<index<Tuple::size - 1>, Tuple>>>;
        };

        template<typename ...G, typename ...Paths, typename ...Starts, typename Visited>
        struct extend_paths<graph<G...>, tuple<Paths...>, tuple<Starts...>, Visited>
        {
            using extended = transform_t<append<graph<G...>>::template one, tuple<Paths...>>;
            using interesting = filter_t<interesting_paths<tuple<Starts...>>::template filter, extended>;
            using filtered = filter_t<remove_visited<Visited>::template filter, extended>;

            using type = unique_t<join_t<interesting, typename extend_paths<
                graph<G...>,
                filtered,
                tuple<Starts...>,
                unique_t<join_t<flatten_t<extended>, Visited>>
            >::type>>;
        };

        template<typename ...G, typename ...Starts, typename Visited>
        struct extend_paths<graph<G...>, tuple<>, tuple<Starts...>, Visited>
        {
            using type = tuple<>;
        };

        template<typename ...G, typename ...Vertices>
        struct find_paths<graph<G...>, tuple<Vertices...>>
        {
            using type = transform_t<first_and_last, typename extend_paths<
                graph<G...>,
                transform_t<tuple_tuple, tuple<Vertices...>>,
                tuple<Vertices...>,
                tuple<Vertices...>
            >::type>;
        };

        template<typename Tuple>
        struct get_second
        {
            static_assert(Tuple::size == 2);
            using type = tuple<get_t<index<1>, Tuple>>;
        };

        template<typename Tuple>
        struct no_incoming_edges
        {
            using has_incoming_edges = unique_t<transform_t<get_second, Tuple>>;

            template<typename T>
            struct filter
            {
                static constexpr bool value = (0 == count_v<T, has_incoming_edges>);
            };
        };

        template<typename Tuple>
        struct not_in_tuple
        {
            template<typename T>
            struct filter
            {
                static constexpr bool value = (0 == count_v<T, Tuple>);
            };
        };

        template<typename Node>
        struct incoming_edges_from
        {
            template<typename T>
            using filter = std::negation<std::is_same<get_t<index<0>, T>, Node>>;
        };

        template<typename Nodes>
        struct outgoing_edges_from
        {
            template<typename T>
            struct filter
            {
                static const bool value = (0 != count_v<get_t<index<0>, T>, Nodes>);
            };
        };

        template<typename Tuple>
        struct remove_zero_cycles
        {
            static constexpr bool value = !std::is_same_v<
                get_t<index<0>, Tuple>,
                get_t<index<1>, Tuple>
            >;
        };

        CT_TYPE(kahn);

        template<typename Tuple, typename Vertices, typename Result, typename Node, typename ...Tail>
        struct kahn<Tuple, Vertices, Result, tuple<Node, Tail...> /* =S */>
        {
            using remaining_edges = unique_t<filter_t<incoming_edges_from<Node>::template filter, Tuple>>;
            using vertices = unique_t<filter_t<no_incoming_edges<remaining_edges>::template filter, Vertices>>;
            using result = append_t<Result, Node>;
            using set = join_t<
                tuple<Tail...>,
                filter_t<
                    not_in_tuple<tuple<Tail...>>::template filter,
                    filter_t<
                        not_in_tuple<result>::template filter,
                        vertices
                    >
                >
            >;

            using type = typename kahn<
                remaining_edges,
                flatten_t<remaining_edges>,
                append_t<Result, Node>,
                set
            >::type;
        };

        template<typename Result>
        struct kahn<tuple<>, tuple<>, Result, tuple<>>
        {
            using type = Result;
        };

        CT_TYPE(bfs);

        template<typename Visiting, typename Visited, typename Edges>
        struct bfs<Visiting, Visited, Edges>
        {
            using connected = transform_t<detail::get_last, filter_t<outgoing_edges_from<Visiting>::template filter, Edges>>;
            using type = typename bfs<
                filter_t<
                    not_in_tuple<Visiting>::template filter,
                    filter_t<
                        not_in_tuple<Visited>::template filter,
                        connected
                    >
                >,
                join_t<Visited, Visiting>,
                Edges
            >::type;
        };

        template<typename Visited, typename Edges>
        struct bfs<tuple<>, Visited, Edges>
        {
            using type = Visited;
        };
    }

    CT_TYPE(topological_sort);

    template<typename ...G, typename ...Vertices>
    struct topological_sort<graph<G...>, tuple<Vertices...>>
    {
        using subgraph = filter_t<detail::remove_zero_cycles, typename detail::find_paths<graph<G...>, tuple<Vertices...>>::type>;
        using type = typename detail::kahn<subgraph, tuple<Vertices...>, tuple<>, filter_t<detail::no_incoming_edges<subgraph>::template filter, tuple<Vertices...>>>::type;
    };

    template<typename G, typename V>
    using topological_sort_t = typename topological_sort<G, V>::type;

    CT_TYPE(reachable_vertices);

    template<typename ...G, typename Vertex>
    struct reachable_vertices<graph<G...>, Vertex>
    {
        using type = typename detail::bfs<tuple<Vertex>, tuple<>,
            typename graph<G...>::edges::keys
        >::type;
    };

    template<typename G, typename V>
    using reachable_vertices_t = typename reachable_vertices<G, V>::type;

#ifdef METACG_ENABLE_STATIC_TESTS            
    namespace test
    {

        static_assert(std::is_same_v<topological_sort_t<
                graph<
#include <ct/test/test_graph>
                >,
                tuple<index<0>, index<2>, index<4>>
            >,
            tuple<index<0>, index<2>, index<4>>
        >);
        static_assert(std::is_same_v<topological_sort_t<
                graph<
#include <ct/test/test_graph>
                >,
                tuple<index<10>, index<14>>
            >,
            tuple<index<10>, index<14>>
        >);

        static_assert(std::is_same_v<topological_sort_t<
                graph<
#include <ct/test/test_graph>
                >,
                tuple<index<0>, index<10>>
            >,
            tuple<index<0>, index<10>>
        >);

        static_assert(std::is_same_v<topological_sort_t<
                graph<
#include <ct/test/test_graph>
                >,
                tuple<index<30>, index<34>, index<35>>
            >,
            tuple<index<34>, index<30>, index<35>>
        >);

        static_assert(std::is_same_v<reachable_vertices_t<
                graph<
#include <ct/test/test_graph>
                >,
                index<10>
            >,
            tuple<index<10>, index<11>, index<12>, index<22>, index<13>, index<20>, index<14>, index<21>>
        >);
    }
#endif    
}

