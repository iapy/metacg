#pragma once
#include <ct/tuple.hpp>

namespace ct
{
    template<template <typename> typename F, typename>
    struct filter;

    template<template <typename> typename F, typename Head, typename ...Tail>
    struct filter<F, tuple<Head, Tail...>>
    {
        using type = std::conditional_t<
            F<Head>::value,
            prepend_t<Head, typename filter<F, tuple<Tail...>>::type>,
            typename filter<F, tuple<Tail...>>::type
        >;
    };

    template<template <typename> typename F>
    struct filter<F, tuple<>>
    {
        using type = tuple<>;
    };

    template<template <typename> typename F, typename T>
    using filter_t = typename filter<F, T>::type;

#ifdef METACG_ENABLE_STATIC_TESTS            
    namespace test
    {
        template<typename T>
        struct test_filter_0
        {
            static constexpr bool value = (T::value % 2) == 0;
        };

        template<typename T>
        struct test_filter_1
        {
            static constexpr bool value = (T::value % 2) == 1;
        };

        static_assert(std::is_same_v<
            filter_t<test_filter_0, tuple<index<0>, index<1>, index<2>, index<3>, index<4>>>,
            tuple<index<0>, index<2>, index<4>>
        >);

        static_assert(std::is_same_v<
            filter_t<test_filter_1, tuple<index<0>, index<1>, index<2>, index<3>, index<4>>>,
            tuple<index<1>, index<3>>
        >);
    }
#endif    
}
