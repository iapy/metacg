#pragma once
#include <ct/tuple.hpp>

namespace ct
{
    template<typename ...Tail>
    struct map
    {
        using keys = tuple<get_t<index<0>, Tail>...>;
        using vals = tuple<get_t<index<1>, Tail>...>;
        static_assert(std::is_same_v<keys, unique_t<keys>>);
    };

    template<typename X, typename ...Tail>
    struct get<X, map<Tail...>>
    {
        using type = get_t<index<index_of_v<X, typename map<Tail...>::keys>>, typename map<Tail...>::vals>;
    };

    template<typename X, typename K, typename V, typename Default, typename ...Tail>
    struct get<X, map<pair<K, V>, Tail...>, Default>
    {
        using type = typename get<X, map<Tail...>, Default>::type;
    };

    template<typename X, typename V, typename Default, typename ...Tail>
    struct get<X, map<pair<X, V>, Tail...>, Default>
    {
        using type = V;
    };

    template<typename X, typename Default>
    struct get<X, map<>, Default>
    {
        using type = Default;
    };

    template<typename ...A, typename ...B>
    struct join<map<A...>, map<B...>>
    {
        using type = map<A..., B...>;
    };

    template<typename A, typename ...B>
    struct prepend<A, map<B...>>
    {
        using type = map<A, B...>;
    };

    template<typename X, typename ...Tail>
    struct count<X, map<Tail...>>
    {
        static constexpr std::size_t value = count_v<X, typename map<Tail...>::keys>;
    };

    template<typename P, typename Q, typename K, typename V, typename ...Tail>
    struct insert<pair<P, Q>, map<pair<K, V>, Tail...>>
    {
        using type = std::conditional_t<
            0 == count_v<P, map<pair<K, V>, Tail...>>,
            map<pair<P, Q>, pair<K, V>, Tail...>,
            std::conditional_t
            <
                std::is_same_v<P, K>,
                map<pair<P, Q>, Tail...>,
                prepend_t<pair<K, V>, typename insert<pair<P, Q>, map<Tail...>>::type>
            >
        >;
    };


    template<typename P, typename Q>
    struct insert<pair<P, Q>, map<>>
    {
        using type = map<pair<P, Q>>;
    };

#ifdef METACG_ENABLE_STATIC_TESTS            
    namespace test
    {
        static_assert(std::is_same_v<
            typename map
            <
                pair<std::uint32_t, uint16_t>,
                pair<std::uint64_t, uint32_t>
            >::keys,
            tuple<std::uint32_t, std::uint64_t>
        >);

        static_assert(std::is_same_v<
            typename map
            <
                pair<std::uint32_t, uint16_t>,
                pair<std::uint64_t, uint32_t>
            >::vals,
            tuple<std::uint16_t, std::uint32_t>
        >);

        static_assert(std::is_same_v<
            typename map
            <
                pair<std::uint32_t, uint16_t>,
                pair<std::uint64_t, uint16_t>
            >::vals,
            tuple<std::uint16_t, std::uint16_t>
        >);

        static_assert(is_same_v<
            get_t
            <
                std::uint32_t,
                map
                <
                    pair<std::uint16_t, std::uint32_t>,
                    pair<std::uint32_t, std::uint64_t>
                >
            >,
            std::uint64_t
        >);

        static_assert(std::is_same_v<
            get_t
            <
                std::uint16_t,
                map
                <
                    pair<std::uint16_t, std::uint32_t>,
                    pair<std::uint32_t, std::uint64_t>
                >
            >,
            std::uint32_t
        >);

        static_assert(std::is_same_v<
            get_t
            <
                std::uint64_t,
                map
                <
                    pair<std::uint16_t, std::uint32_t>,
                    pair<std::uint32_t, std::uint64_t>
                >,
                void
            >,
            void
        >);


        static_assert(
            0 == count_v<
                std::uint8_t,
                map
                <
                    pair<std::uint16_t, std::uint32_t>,
                    pair<std::uint32_t, std::uint64_t>
                >
            >
        );

        static_assert(
            1 == count_v<
                std::uint16_t,
                map
                <
                    pair<std::uint16_t, std::uint32_t>,
                    pair<std::uint32_t, std::uint64_t>
                >
            >
        );

        static_assert(std::is_same_v<
            insert_t<
                pair<std::uint16_t, std::uint32_t>,
                map<>
            >,
            map<pair<std::uint16_t, std::uint32_t>>
        >);

        static_assert(std::is_same_v<
            insert_t<
                pair<std::uint16_t, std::uint32_t>,
                map<pair<std::uint16_t, std::uint64_t>>
            >,
            map<pair<std::uint16_t, std::uint32_t>>
        >);
    }
#endif    
}

