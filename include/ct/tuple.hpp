#pragma once
#include <tuple>
#include <cstdint>
#include <type_traits>
#include <ct/utils.hpp>

namespace ct
{
    CT_TYPE(tuple)
    {
        using std_type = std::tuple<Tail...>;
        static constexpr std::size_t size = sizeof...(Tail);
    };

    template<typename A, typename B>
    using pair = tuple<A, B>;

    template<typename ...A, typename ...B>
    struct join<tuple<A...>, tuple<B...>>
    {
        using type = tuple<A..., B...>;
    };

    template<typename ...A, typename B>
    struct append<tuple<A...>, B>
    {
        using type = tuple<A..., B>;
    };

    template<typename A, typename ...B>
    struct prepend<A, tuple<B...>>
    {
        using type = tuple<A, B...>;
    };

    template<typename A, typename B>
    using join_t = typename join<A, B>::type;

#ifdef METACG_ENABLE_STATIC_TESTS            
    namespace test
    {
        using namespace std;
        static_assert(is_same_v<
            tuple<uint32_t, uint64_t, uint16_t, uint64_t>,
            join_t<tuple<uint32_t, uint64_t>, tuple<uint16_t, uint64_t>>
        >);
        static_assert(is_same_v<
            tuple<>,
            join_t<tuple<>, tuple<>>
        >);
        static_assert(is_same_v<
            tuple<uint32_t>,
            prepend_t<uint32_t, tuple<>>
        >);
        static_assert(is_same_v<
            tuple<uint32_t>,
            append_t<tuple<>, uint32_t>
        >);
        static_assert(is_same_v<
            tuple<uint32_t, uint64_t>,
            prepend_t<uint32_t, tuple<uint64_t>>
        >);
        static_assert(is_same_v<
            tuple<uint32_t, uint64_t>,
            append_t<tuple<uint32_t>, uint64_t>
        >);
    }
#endif

    template<std::size_t I, typename Head, typename ...Tail>
    struct get<index<I>, tuple<Head, Tail...>>
    {
        using type = typename get<index<I - 1>, tuple<Tail...>>::type;
    };

    template<typename Head, typename ...Tail>
    struct get<index<0>, tuple<Head, Tail...>>
    {
        using type = Head;
    };

#ifdef METACG_ENABLE_STATIC_TESTS
    namespace test
    {
        using namespace std;
        static_assert(is_same_v<
            std::uint16_t,
            get_t<index<0>, tuple<std::uint16_t, std::uint32_t, std::uint64_t>>
        >);
        static_assert(is_same_v<
            std::uint32_t,
            get_t<index<1>, tuple<std::uint16_t, std::uint32_t, std::uint64_t>>
        >);
        static_assert(is_same_v<
            std::uint64_t,
            get_t<index<2>, tuple<std::uint16_t, std::uint32_t, std::uint64_t>>
        >);
    }
#endif

    namespace detail
    {
        template<typename T, typename R>
        struct reverse;

        template<typename H, typename ...T, typename ...R>
        struct reverse<tuple<H, T...>, tuple<R...>> : reverse<tuple<T...>, tuple<H, R...>>
        {
        };

        template<typename ...R>
        struct reverse<tuple<>, tuple<R...>>
        {
            using type = tuple<R...>;
        };
    }

    template<typename T>
    using reverse_t = typename detail::reverse<T, tuple<>>::type;

#ifdef METACG_ENABLE_STATIC_TESTS
    namespace test
    {
        using namespace std;
        static_assert(is_same_v<
            tuple<uint64_t, uint32_t, uint16_t>,
            reverse_t<tuple<uint16_t, uint32_t, uint64_t>>
        >);
    }
#endif    

    template<typename T, typename ...Tail>
    struct count<T, tuple<Tail...>>
    {
        static constexpr std::size_t value = sum(std::integer_sequence<std::size_t, std::is_same_v<T, Tail>...>{});
    };

    template<typename T, typename Tp>
    static constexpr std::size_t count_v = count<T, Tp>::value;

#ifdef METACG_ENABLE_STATIC_TESTS            
    namespace test
    {
        static_assert(count_v<std::uint16_t, tuple<>> == 0);
        static_assert(count_v<std::uint16_t, tuple<std::uint16_t>> == 1);
        static_assert(count_v<std::uint16_t, tuple<std::uint16_t, std::uint16_t>> == 2);
        static_assert(count_v<std::uint16_t, tuple<std::uint16_t, std::uint32_t>> == 1);
        static_assert(count_v<std::uint16_t, tuple<std::uint32_t, std::uint16_t>> == 1);
        static_assert(count_v<std::uint64_t, tuple<std::uint32_t, std::uint16_t>> == 0);
    }
#endif    

    namespace detail
    {
        template<typename T, typename U, typename E = void>
        struct unique;

        template<typename H, typename ...T, typename ...U>
        struct unique<tuple<H, T...>, tuple<U...>, std::enable_if_t<0 == count_v<H, tuple<U...>>>> : unique<tuple<T...>, tuple<U..., H>>
        {
        };

        template<typename H, typename ...T, typename ...U>
        struct unique<tuple<H, T...>, tuple<U...>, std::enable_if_t<1 == count_v<H, tuple<U...>>>> : unique<tuple<T...>, tuple<U...>>
        {
        };

        template<typename ...U>
        struct unique<tuple<>, tuple<U...>>
        {
            using type = tuple<U...>;
        };
    }

    template<typename T>
    using unique_t = typename detail::unique<T, tuple<>>::type;

#ifdef METACG_ENABLE_STATIC_TESTS
    namespace test
    {
        using namespace std;
        static_assert(is_same_v<
            tuple<std::uint16_t, std::uint32_t>,
            unique_t<tuple<std::uint16_t, std::uint32_t, std::uint16_t>>
        >);
        static_assert(is_same_v<
            tuple<std::uint16_t, std::uint32_t>,
            unique_t<tuple<std::uint16_t, std::uint32_t, std::uint32_t, std::uint16_t>>
        >);
    }
#endif    

    CT_TYPE(index_of);

    template<typename T, typename Head, typename ...Tail>
    struct index_of<T, tuple<Head, Tail...>>
    {
        static constexpr std::size_t value = 1 + index_of<T, tuple<Tail...>>::value;
    };

    template<typename T, typename ...Tail>
    struct index_of<T, tuple<T, Tail...>> : index<0> {};

    template<typename T, typename V>
    static constexpr std::size_t index_of_v = index_of<T,V>::value;

#ifdef METACG_ENABLE_STATIC_TESTS
    namespace test
    {
        static_assert(0 == index_of_v<
            std::uint16_t,
            tuple<std::uint16_t, std::uint32_t, std::uint64_t>
        >);

        static_assert(1 == index_of_v<
            std::uint32_t,
            tuple<std::uint16_t, std::uint32_t, std::uint64_t>
        >);

        static_assert(2 == index_of_v<
            std::uint64_t,
            tuple<std::uint16_t, std::uint32_t, std::uint64_t>
        >);
    }
#endif
}

