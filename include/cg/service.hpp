#pragma once
#include <cg/host.hpp>
#include <ct/filter.hpp>

#include <array>
#include <thread>
#include <condition_variable>

namespace cg
{
    namespace detail
    {
        template<typename C>
        struct get_service
        {
            template<typename S>
            struct make_tuple
            {
                using type = ct::tuple<ct::pair<C, S>>;
            };

            using type = ct::transform_t<make_tuple, typename C::Services>;
        };

        template<typename C>
        struct with_services
        {
            static constexpr bool value = (0 != C::Services::size);
        };

        CT_TYPE(threads);

        template<std::size_t ...Is>
        struct threads<std::index_sequence<Is...>>
        {
            template<typename F>
            threads(F &&f) : threads_{f(std::integral_constant<std::size_t, Is>{}, codes_[Is])...} {}

            int join(int mt)
            {
                for(std::size_t i = 0; i < sizeof ...(Is); ++i)
                {
                    threads_[i].join();
                }
                return std::any_of(codes_.begin(), codes_.end(), [](auto rc){ return rc != 0; }) ? 1 : mt;
            }

        private:
            std::array<int, sizeof ... (Is)> codes_;
            std::array<std::thread, sizeof ... (Is)> threads_;
        };
    }

    template<typename ...Tail>
    int Service(Host<Graph<Tail...>> &host)
    {
        using Services = ct::transform_t<detail::get_service,
            ct::reverse_t<ct::topological_sort_t<
                typename Graph<Tail...>::type,
                ct::filter_t<detail::with_services, typename Graph<Tail...>::type::vertices>
            >>
        >;

        if constexpr (Services::size == 0)
        {
            return 0;
        }
        else if constexpr (Services::size == 1)
        {
            using C = ct::get_t<ct::index<0>, ct::get_t<ct::index<0>, Services>>;
            using I = ct::get_t<ct::index<1>, ct::get_t<ct::index<0>, Services>>;
            static_assert(0 == ct::count_v<I, typename C::Ports::vals>);
            return host.template get<C, I, ct::map<ct::pair<I, I>>>().run();
        }
        else
        {
            std::mutex mu;
            std::size_t next = 0;
            std::condition_variable cv;

            auto threads = detail::threads<detail::make_index_range<0, Services::size - 1>>{[&host, &mu, &cv, &next](auto index, int &rc){
                using C = ct::get_t<ct::index<0>, ct::get_t<ct::index<decltype(index)::value + 1>, Services>>;
                using I = ct::get_t<ct::index<1>, ct::get_t<ct::index<decltype(index)::value + 1>, Services>>;
                static_assert(0 == ct::count_v<I, typename C::Ports::vals>);
                return std::thread{
                    [&host, &rc, &mu, &cv, &next] {
                        std::unique_lock<std::mutex> guard(mu);
                        cv.wait(guard, [&next]{ return next == decltype(index)::value + 1; });
                        auto component = host.template get<C, I, ct::map<ct::pair<I, I>>>();
                        next = decltype(index)::value + 2;
                        cv.notify_all();
                        guard.unlock();
                        rc = component.run();
                    }
                };
            }};

            using C = ct::get_t<ct::index<0>, ct::get_t<ct::index<0>, Services>>;
            using I = ct::get_t<ct::index<1>, ct::get_t<ct::index<0>, Services>>;
            auto component = host.template get<C, I, ct::map<ct::pair<I, I>>>();
            {
                std::unique_lock<std::mutex> guard(mu);
                next = 1;
            }
            cv.notify_all();
            return threads.join(component.run());
        }
    }
}


