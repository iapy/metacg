#pragma once

#include <ct/map.hpp>
#include <ct/utils.hpp>

namespace cg {
namespace detail {

struct Component
{
    template<typename> struct Types {};
    template<typename> struct State
    {
        State() = default;
        State(State &&) = default;
        State(State const &) = delete;
    };
};

}// namespace detail

struct NullResolver
{
    template<typename Tag> using Component = void;
    template<typename Tag> using TypesTuple = ct::tuple<>;
    template<typename Tag> using ReverseTypesTuple = ct::tuple<>;
};

struct Component : detail::Component
{
    using Ports = ct::map<>;
    using Services = ct::tuple<>;
    using Initializers = ct::tuple<>;
};

template<typename T>
constexpr bool IsComponent_v = std::is_base_of_v<Component, T>;

namespace test
{
    static_assert( IsComponent_v<Component>);
    static_assert(!IsComponent_v<NullResolver>);
}
} // namespace cg

