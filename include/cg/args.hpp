#pragma once
#include <utility>
#include <functional>

namespace cg
{
    namespace detail
    {
        template<typename Component, typename ...Tail>
        class Args : private std::tuple<Tail...>
        {
        public:
            Args(Tail&& ...args) : std::tuple<Tail...>{std::forward<Tail>(args)...} {}

            template<typename State>
            auto make()
            {
                return make<State>(std::make_index_sequence<sizeof ...(Tail)>{});
            }
        private:
            template<typename State, std::size_t ...Is>
            auto make(std::index_sequence<Is...>)
            {
                return State{std::forward<std::tuple_element_t<Is, std::tuple<Tail...>>>(std::get<Is>(*this))...};
            }
        };
    }
    
    template<typename Component, typename ...Tail>
    auto Args(Tail&&... args)
    {
        return detail::Args<Component, Tail...>{std::forward<Tail>(args)...};
    }
}

