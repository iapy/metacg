#pragma once

#include <cg/component.hpp>
#include <ct/tuple.hpp>
#include <ct/graph.hpp>

namespace cg
{
    CT_TYPE(Pipeline);

    template<typename T>
    static constexpr bool IsPipeline_v = false;

    template<typename ...Tail>
    static constexpr bool IsPipeline_v<Pipeline<Tail...>> = true;

    CT_TYPE(Group)
    {
        using components = ct::tuple<Tail...>;
    };

    namespace detail
    {
        template<typename T>
        static constexpr bool IsGroup_v = false;

        template<typename ...Tail>
        static constexpr bool IsGroup_v<Group<Tail...>> = true;

        template<typename M>
        struct only_port
        {
            static_assert(1 == M::keys::size);
            using type = ct::get_t<ct::index<0>, typename M::keys>;
        };

        template<typename A, typename T>
        struct group
        {
            template<typename X>
            struct fold_r
            {
                using type = ct::tuple<ct::edge<A, X, T>>;
            };

            template<typename X>
            struct fold_l
            {
                using type = ct::tuple<ct::edge<X, A, T>>;
            };
        };

        template<typename A, typename B, typename T = void, typename Enable = void>
        struct PipelineTail;

        template<typename A, typename B, typename T>
        struct PipelineTail<A, B, T, std::enable_if_t<IsComponent_v<T>>>
        {
            using tag = typename only_port<typename B::Ports>::type;
            using edges = ct::tuple<ct::edge<A, B, tag>, ct::edge<B, T, tag>>;
        };

        template<typename A, typename B, typename T>
        struct PipelineTail<A, B, T, std::enable_if_t<
            (!IsGroup_v<A> && !IsPipeline_v<A>)
         && IsPipeline_v<B>
         && !IsComponent_v<T>
         && !std::is_same_v<T, void>
        >>
        {
            using S = typename ct::get_t<ct::index<0>, typename B::edges>::A;
            using tag = T;
            using edges = ct::join_t<ct::tuple<ct::edge<A, S, T>>, typename B::edges>;
        };

        template<typename A, typename B, typename T>
        struct PipelineTail<A, B, T, std::enable_if_t<
            (!IsGroup_v<A> && !IsPipeline_v<A>)
         && IsGroup_v<B>
         && !IsComponent_v<T>
         && !std::is_same_v<T, void>
        >>
        {
            using tag = T;
            using edges = ct::transform_t<group<A, tag>::template fold_r, typename B::components>;
        };

        template<typename A, typename B, typename T>
        struct PipelineTail<A, B, T, std::enable_if_t<
            IsGroup_v<A>
         && (!IsGroup_v<B> && !IsPipeline_v<B>)
         && !IsComponent_v<T>
         && !std::is_same_v<T, void>
        >>
        {
            using tag = T;
            using edges = ct::transform_t<group<B, tag>::template fold_l, typename A::components>;
        };

        template<typename A, typename B, typename T>
        struct PipelineTail<A, B, T, std::enable_if_t<IsPipeline_v<T>>>
        {
            using tag = typename T::tag;
            using edges = ct::join_t<
                ct::tuple<
                    ct::edge<A, B, tag>,
                    ct::edge<B, typename ct::get_t<ct::index<0>, typename T::edges>::A, tag>
                >,
                typename T::edges
            >;
        };

        template<typename A, typename B, typename T>
        struct PipelineTail<A, B, T, std::enable_if_t<
            (!IsPipeline_v<A> && !IsGroup_v<A>)
         && (!IsPipeline_v<B> && !IsGroup_v<B>)
         && !IsComponent_v<T>
         && !IsPipeline_v<T> 
         && !std::is_same_v<T, void>
        >>
        {
            using tag = T;
            using edges = ct::tuple<ct::edge<A, B, tag>>;
        };

        template<typename A, typename B>
        struct PipelineTail<A, B, void, std::enable_if_t<
            (!IsPipeline_v<A> && !IsGroup_v<A>)
         && IsComponent_v<B>
        >>
        {
            using tag = typename only_port<typename B::Ports>::type;
            using edges = ct::tuple<ct::edge<A, B, tag>>;
        };

        template<typename A, typename B>
        struct PipelineTail<A, B, void, std::enable_if_t<
            (!IsPipeline_v<A> && !IsGroup_v<A>)
        &&  IsGroup_v<B>
        >>
        {
            using tag = typename only_port<typename ct::get_t<ct::index<0>, typename B::components>::Ports>::type;
            using edges = ct::transform_t<group<A, tag>::template fold_r, typename B::components>;
        };

        template<typename A, typename B>
        struct PipelineTail<A, B, void, std::enable_if_t<
            IsGroup_v<A>
        && (!IsPipeline_v<B> && !IsGroup_v<B>)
        >>
        {
            using tag = typename only_port<typename B::Ports>::type;
            using edges = ct::transform_t<group<B, tag>::template fold_l, typename A::components>;
        };

        template<typename A, typename B>
        struct PipelineTail<A, B, void, std::enable_if_t<
            (!IsPipeline_v<A> && !IsGroup_v<A>)
         && IsPipeline_v<B>
        >>
        {
            using S = typename ct::get_t<ct::index<0>, typename B::edges>::A;
            using tag = typename only_port<typename S::Ports>::type;
            using edges = ct::prepend_t<ct::edge<A, S, tag>, typename B::edges>;
        };
    }

    template<typename A, typename B, typename C, typename ...Tail>
    struct Pipeline<A, B, C, Tail...>
    {
        using tag = typename Pipeline<B, C, Tail...>::tag;
        using edges = ct::prepend_t<ct::edge<A, B, tag>, typename Pipeline<B, C, Tail...>::edges>;
    };

    template<typename A, typename B, typename T>
    struct Pipeline<A, B, T> : detail::PipelineTail<A, B, T> {};

    template<typename A, typename B>
    struct Pipeline<A, B> : detail::PipelineTail<A, B> {};

    template<typename A, typename B, typename T = void>
    using Connect = Pipeline<A, B, T>;
}

