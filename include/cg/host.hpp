#pragma once

#include <ct/map.hpp>

#include <cg/args.hpp>
#include <cg/graph.hpp>

#include <filesystem>

namespace cg
{
    CT_TYPE(Host);

    namespace detail
    {
        CT_TYPE(index_of);

        template<typename T, typename A, typename ...X, typename ...Tail>
        struct index_of<T, detail::Args<A, X...>, Tail...>
        {
            static constexpr std::size_t value = std::is_same_v<A, std::decay_t<T>> ? 0 : 1 + index_of<T, Tail...>::value;
        };

        template<typename T>
        struct index_of<T>
        {
            static constexpr std::size_t value = 0;
        };

        template<size_t B, size_t E, size_t ...Is>
        struct make_index_range_helper
        {
            using type = typename make_index_range_helper<B + 1, E, Is..., B>::type;
        };

        template<size_t B, size_t ...Is>
        struct make_index_range_helper<B, B, Is...>
        {
            using type = std::index_sequence<Is...>;
        };

        template<size_t A, size_t B>
        using make_index_range = typename make_index_range_helper<A, B>::type;

#ifdef METACG_ENABLE_STATIC_TESTS            
        namespace test
        {
            using namespace std;
            static_assert(is_same_v<make_index_range<0, 0>, index_sequence<>>);
            static_assert(is_same_v<make_index_range<0, 1>, index_sequence<0>>);
            static_assert(is_same_v<make_index_range<0, 2>, index_sequence<0, 1>>);
            static_assert(is_same_v<make_index_range<1, 3>, index_sequence<1, 2>>);
            static_assert(is_same_v<make_index_range<4, 4>, index_sequence<>>);
        }
#endif            

        template<typename States, typename Components>
        struct construct
        {
            static_assert(std::tuple_size_v<States> == std::tuple_size_v<Components>);

            template<
                std::size_t I,
                typename T,
                std::size_t ...P1,
                std::size_t ...P2,
                std::size_t ...P3,
                std::size_t ...P4
            >
            static States from_(T&& t, std::index_sequence<P1...>, std::index_sequence<P2...>, std::index_sequence<P3...>, std::index_sequence<P4...>)
            {
                return from<I>(std::get<P1>(t)..., std::get<P2>(t)..., std::get<P3>(t)..., std::get<P4>(t)...);
            }

            template<typename ...Tail, std::size_t ...I>
            static States from_(std::tuple<Tail...>&& args, std::index_sequence<I...>)
            {
                return States{std::get<I>(args).template make<std::tuple_element_t<I, States>>()...};
            }

            template<std::size_t I, typename ...Tail>
            static States from(Tail&& ...args)
            {
                if constexpr (I == std::tuple_size_v<States>)
                {
                    // tail of recursion
                    return from_(std::forward_as_tuple(args...), make_index_range<0, std::tuple_size_v<States>>{});
                }
                else if constexpr (I == sizeof...(Tail))
                {
                    using S = std::tuple_element_t<I, States>;
                    return from<I + 1>(std::forward<Tail>(args)..., Args<S>{});
                }
                else
                {
                    using S = std::tuple_element_t<I, States>;
                    using C = std::tuple_element_t<I, Components>;
                    static constexpr auto J = index_of<C, std::decay_t<Tail>...>::value;

                    if constexpr (J == sizeof...(Tail)) 
                    {
                        return from_<I + 1>(
                            std::forward_as_tuple(Args<C>{}, args...),
                            make_index_range<1, I + 1>{},
                            make_index_range<0, 1>{},
                            make_index_range<I + 1, 1 + sizeof...(Tail)>{},
                            std::index_sequence<>{}
                        );
                    }
                    else if constexpr (I == J)
                    {
                        return from<I + 1>(std::forward<Tail>(args)...);
                    }
                    else
                    {
                        static_assert(I <= J);
                        return from_<I + 1>(
                            std::forward_as_tuple(args...),
                            make_index_range<0, I>{},
                            make_index_range<J, J + 1>{},
                            make_index_range<I, J>{},
                            make_index_range<J + 1, sizeof...(Tail)>{}
                        );
                    }
                }
            }
        };

        template<typename X, typename Y, typename M, typename E = void>
        struct get_connection;

        template<typename X, typename Y, typename A, typename B, typename Tuple, typename ...Tail>
        struct get_connection<X, Y, ct::map<ct::tuple<ct::tuple<A, B>, Tuple>, Tail...>, std::enable_if_t<std::is_same_v<A, X> && 0 != ct::count_v<Y, Tuple>>>
        {
            using next = ct::map<Tail...>;
            using type = B;
        };

        template<typename X, typename Y, typename A, typename B, typename Tuple, typename ...Tail>
        struct get_connection<X, Y, ct::map<ct::tuple<ct::tuple<A, B>, Tuple>, Tail...>, std::enable_if_t<!std::is_same_v<A, X> || 0 == ct::count_v<Y, Tuple>>>
        : get_connection<X, Y, ct::map<Tail...>>
        {
        };

        template<typename X, typename Y>
        struct get_connection<X, Y, ct::map<>>
        {
            using next = ct::map<>;
            using type = void;
        };

        template<typename X, typename Y, typename M, typename E = void>
        struct get_connected;

        template<typename X, typename Y, typename A, typename B, typename Tuple, typename ...Tail>
        struct get_connected<X, Y, ct::map<ct::tuple<ct::tuple<A, B>, Tuple>, Tail...>, std::enable_if_t<std::is_same_v<B, X> && 0 != ct::count_v<Y, Tuple>>>
        {
            using next = ct::map<Tail...>;
            using type = A;
        };

        template<typename X, typename Y, typename A, typename B, typename Tuple, typename ...Tail>
        struct get_connected<X, Y, ct::map<ct::tuple<ct::tuple<A, B>, Tuple>, Tail...>, std::enable_if_t<!std::is_same_v<B, X> || 0 == ct::count_v<Y, Tuple>>>
        : get_connected<X, Y, ct::map<Tail...>>
        {
        };

        template<typename X, typename Y>
        struct get_connected<X, Y, ct::map<>>
        {
            using next = ct::map<>;
            using type = void;
        };

        template<template <typename> typename Resolver, typename C, typename Edges>
        struct get_types
        {
            template<typename Tag>
            using Remote = typename detail::get_connection<C, Tag, Edges>;

            template<typename Tag>
            using types = ct::join_t<
                typename get_types<Resolver, typename Remote<Tag>::type, void>::template types<Tag>,
                typename get_types<Resolver, C, typename Remote<Tag>::next>::template types<Tag>
            >;
        };

        template<template <typename> typename Resolver, typename C>
        struct get_types<Resolver, C, ct::map<>>
        {
            template<typename Tag>
            using types = ct::tuple<>;
        };

        template<template <typename> typename Resolver, typename C>
        struct get_types<Resolver, C, void>
        {
            template<typename Tag>
            using types = ct::tuple<typename C::template Types<Resolver<C>>>;
        };

        template<template <typename> typename Resolver>
        struct get_types<Resolver, void, void>
        {
            template<typename Tag>
            using types = ct::tuple<>;
        };

        template<template <typename> typename Resolver, typename C, typename Edges>
        struct get_types_reverse
        {
            template<typename Tag>
            using Remote = typename detail::get_connected<C, Tag, Edges>;

            template<typename Tag>
            using types = ct::join_t<
                typename get_types_reverse<Resolver, typename Remote<Tag>::type, void>::template types<Tag>,
                typename get_types_reverse<Resolver, C, typename Remote<Tag>::next>::template types<Tag>
            >;
        };

        template<template <typename> typename Resolver, typename C>
        struct get_types_reverse<Resolver, C, ct::map<>>
        {
            template<typename Tag>
            using types = ct::tuple<>;
        };

        template<template <typename> typename Resolver, typename C>
        struct get_types_reverse<Resolver, C, void>
        {
            template<typename Tag>
            using types = ct::tuple<typename C::template Types<Resolver<C>>>;
        };

        template<template <typename> typename Resolver>
        struct get_types_reverse<Resolver, void, void>
        {
            template<typename Tag>
            using types = ct::tuple<>;
        };

        template<typename C, typename S>
        struct State
        {
            template<typename ...T>
            State(T&& ...args) : state_{std::forward<T>(args)...} {}

            S state_;
        };

        template<typename>
        struct Initializers;

        template<typename ...T>
        struct Initializers<std::tuple<T...>> : std::tuple<T...>
        {
            template<typename H>
            Initializers(H *host) : std::tuple<T...>{T{host}...} {}
        };
    }

    template<typename ...Tail>
    int Service(Host<Graph<Tail...>> &g);

    template<typename ...Tail>
    class Host<Graph<Tail...>>
    {
        template<typename C>
        class Resolver
        {
            template<typename Tag>
            using Component = typename detail::get_connection<C, Tag, typename Graph<Tail...>::type::edges>::type;
        public:
            template<typename Tag>
            using TypesTuple = typename detail::get_types<Resolver, C, typename Graph<Tail...>::type::edges>::template types<Tag>;

            template<typename Tag>
            using ReverseTypesTuple = typename detail::get_types_reverse<Resolver, C, typename Graph<Tail...>::type::edges>::template types<Tag>;

            template<typename Tag>
            using Types = typename Component<Tag>::template Types<Resolver<Component<Tag>>>;

            template<typename Tag>
            using ReverseTypes = std::enable_if_t<(1 == ReverseTypesTuple<Tag>::size), ct::get_t<ct::index<0>, ReverseTypesTuple<Tag>>>;
        };

        template<typename C>
        struct get_state
        {
            using type = ct::tuple<detail::State<C, typename C::template State<typename C::template Types<Resolver<C>>>>>;
        };

        using Components = typename Graph<Tail...>::type::vertices::std_type;
        using States = typename ct::transform_t<get_state, typename Graph<Tail...>::type::vertices>::std_type;

        States states;

        template<typename C>
        struct InterfaceBase
        {
            decltype(ct::get_t<ct::index<0>, typename get_state<C>::type>::state_) &state;
            Host<Graph<Tail...>> *self;
        };

        template<typename C>
        class Interface
        : private InterfaceBase<C>
        , public C::template Types<typename Host<Graph<Tail...>>::template Resolver<C>>
        {
        private:
            template<typename Edges, typename Tag, typename Lambda>
            void apply_(Tag, Lambda &&lambda)
            {
                using Remote = typename detail::get_connection<C, Tag, Edges>;
                if constexpr (!std::is_same_v<void, typename Remote::type>)
                {
                    lambda(this->self->template get<typename Remote::type, Tag>());
                    apply_<typename Remote::next>(Tag{}, std::forward<Lambda>(lambda));
                }
            }

            template<typename Edges, typename Tag, typename Value, typename Lambda>
            auto apply_(Tag, Value &&value, Lambda &&lambda)
            {
                using Remote = typename detail::get_connection<C, Tag, Edges>;
                if constexpr (!std::is_same_v<void, typename Remote::type>)
                {
                    return apply_<typename Remote::next>(
                        Tag{},
                        lambda(this->self->template get<typename Remote::type, Tag>(), std::forward<Value>(value)),
                        std::forward<Lambda>(lambda));
                }
                else
                {
                    return value;
                }
            }

            template<typename Edges, typename Tag>
            static constexpr std::size_t remotes_()
            {
                using Remote = typename detail::get_connection<C, Tag, Edges>;
                if constexpr (!std::is_same_v<void, typename Remote::type>)
                {
                    return 1 + remotes_<typename Remote::next, Tag>();
                }
                else
                {
                    return 0;
                }
            }

        protected:
            template<typename Tag>
            auto remote(Tag)
            {
                using Remote = typename detail::get_connection<C, Tag, typename Graph<Tail...>::type::edges>;
                static_assert(std::is_same_v<void, typename detail::get_connection<C, Tag, typename Remote::next>::type>, "Multiple connections. Use apply.");

                return this->self->template get<typename Remote::type, Tag>();
            }

            template<typename Tag, typename Lambda>
            void apply(Tag, Lambda &&lambda)
            {
                apply_<typename Graph<Tail...>::type::edges>(Tag{}, std::forward<Lambda>(lambda));
            }

            template<typename Tag, typename Value, typename Lambda>
            auto apply(Tag, Value &&value, Lambda &&lambda)
            {
                return apply_<typename Graph<Tail...>::type::edges>(Tag{}, std::forward<Value>(value), std::forward<Lambda>(lambda));
            }

            template<typename Tag>
            auto local(Tag)
            {
                return this->self->template get<C, Tag>();
            }

            template<typename Tag>
            static constexpr bool has_remote = !std::is_same_v<void, typename detail::get_connection<C, Tag, typename Graph<Tail...>::type::edges>::type>;

            template<typename Tag>
            static constexpr std::size_t num_remotes = remotes_<typename Graph<Tail...>::type::edges, Tag>();

            using InterfaceBase<C>::state;
        };

        template<typename C>
        struct get_initializers
        {
            template<typename X>
            struct transform
            {
                struct base : X::template Interface<Interface<C>>
                {
                    base(Host<Graph<Tail...>> *self) : X::template Interface<Interface<C>>{[self]{
                        using State = ct::get_t<ct::index<0>, typename get_state<C>::type>;
                        InterfaceBase<C> state{std::get<State>(self->states).state_, self};
                        return *reinterpret_cast<Interface<C>*>(&state);
                    }()} {}
                };

                using type = ct::tuple<base>;
            };

            using type = ct::transform_t<transform, typename C::Initializers>;
        };
        
        using Initializers = typename ct::transform_t<get_initializers, typename Graph<Tail...>::type::vertices>::std_type;
        detail::Initializers<Initializers> initializers;

        template<typename C, typename I, typename P = typename C::Ports>
        auto get()
        {
            using Instance = typename ct::get_t<I, P, void>::template Interface<Interface<C>>;
            static_assert(sizeof(Instance) == sizeof(Interface<C>));

            using State = ct::get_t<ct::index<0>, typename get_state<C>::type>;

            InterfaceBase<C> state{std::get<State>(states).state_, this};
            return Instance{std::move(*reinterpret_cast<Interface<C>*>(&state))};
        }

        friend int Service<>(Host<Graph<Tail...>>&);

    public:
        template<typename ...T> Host(std::filesystem::path &&, T&&...);
        template<typename ...T> Host(std::filesystem::path const &, T&&...);

        template<typename ...T>
        Host(T&& ...args)
        : states{detail::construct<States, Components>::template from<0>(std::forward<T>(args)...)}
        , initializers(this)
        {}
    };
}

