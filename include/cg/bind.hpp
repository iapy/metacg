#pragma once
#include <utility>
#include <functional>
#include <ct/tuple.hpp>

namespace cg {
namespace detail {

template<typename Interface>
class Invoker
{
public:
    template<typename Base>
    Invoker(Base *context) : context{*static_cast<Interface*>(context)}
    {
        static_assert(std::is_base_of_v<Base, Interface>);
    }

    template<typename Result, typename ...Args, typename ...Xrgs>
    auto operator () (Result (Interface::*fn)(Args...), Xrgs&&... args) const
    {
        return (context.*fn)(std::forward<Xrgs>(args)...);
    }

private:
    mutable Interface context;
};

template<typename T>
struct ref
{
    using type = std::reference_wrapper<T const>;
};

template<typename T>
struct ref<std::reference_wrapper<T>>
{
    using type = std::reference_wrapper<T>;
};

} // namespace detail

template<template <typename> typename Interface, typename Base>
struct Bind : Base
{
    template<typename R, typename O, typename ...As, typename ...Bs>
    auto bind(R (O::*fn)(As...), Bs... bound)
    {
        return bind_(ct::tuple<void, As...>{}, fn, std::forward<Bs>(bound)...);
    }
private:
    template<typename T, typename ...Ts, typename R, typename O, typename ...As, typename ...Bs>
    auto bind_(ct::tuple<T, Ts...>, R (O::*fn)(As...), Bs... bound)
    {
        static_assert(std::is_base_of_v<O, Interface<Base>>);
        if constexpr (sizeof ...(As) == sizeof ...(Bs) + sizeof ...(Ts))
        {
            return [=, impl=detail::Invoker<O>(this)](Ts... args) {
                return impl(fn, typename detail::ref<Bs>::type(bound)..., std::forward<Ts>(args)...);
            };
        }
        else
        {
            return bind_(ct::tuple<Ts...>{}, fn, std::forward<Bs>(bound)...);
        }
    }
};

} // namespace cg

