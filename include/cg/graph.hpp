#pragma once

#include <ct/apply.hpp>
#include <ct/graph.hpp>
#include <ct/transform.hpp>

#include <cg/connect.hpp>
#include <cg/component.hpp>

namespace cg
{
    CT_TYPE(Graph);

    namespace detail
    {
        template<typename T, typename X = void>
        struct convert;

        template<typename T>
        struct convert<T, std::enable_if_t<IsComponent_v<T>>>
        {
            using type = ct::tuple<T>;
        };

        template<typename T>
        struct convert<T, std::enable_if_t<IsPipeline_v<T>>>
        {
            using type = typename T::edges;
        };

        template<typename ...Types>
        struct convert<cg::Graph<Types...>>
        {
            using type = ct::transform_t<convert, ct::tuple<Types...>>;
        };
    }

    template<typename ...Types>
    struct Graph
    {
        using type = ct::apply_t<ct::graph, ct::transform_t<detail::convert, ct::tuple<Types...>>>;
    };
}

